INSTALLATION INSTRUCTION
========================

ASSUMPTIONS : 
*************

1. You have a mysql Server Running, if not download & install version 5.6 or later
3. You have imported edxapp database of openedx LMS server in your local MySQL server.
2. You have java version 7 or higher installed on your computer & JAVA_HOME is defined
3. You have Mongo DB server installed on you computer & have cs_comment_service & modulestore (for earlier version of OpenEdx ) or 
   modulestore.active_versions & modulestore.structures (for later version of OpenEdx) installed on your computer.
4. You have all openedx log files copied in your computer say YOUR_LOG_DIR_FULL_PATH directory. To make the application take logs from your 
   direcory see the section "SETTING UP LOGFILE DIRECTORY SECTION".

NOTE:
*****

As stated above, this application needs the following databases & log files from openedx. All these backups need to be of same date, else data will not be consistent.

  > MySQL edxapp Db
  > Mongo DB cs_comments_service
  > MongoDB modulestore or modulestore.active_versions & modulestore.structures depending on the version
  > All log files 


CREATION OF DATABASE:
*********************

1. Copy IITBxDataAnalytics_schema.sql file from 'installation_instructions/files' Directory.
2. Run mysql -u root -p < IITBxDataAnalytics_schema.sql
3. The application assumes you have a 'root' user with no password. If you have different user & password for mysql database follow the 
   instruction given under "CHANGING MYSQL USERID & PASSWORD IN THE APPLICATION" section.


INSTALLING TOMCAT:
*****************

1. Go to the directory of your choice (DIRECTORY_OF_YOUR_CHOICE) where you want to install tomcat. Copy the apache-tomcat-8.0.32.tar.gz 
   from 'installation_instructions/files' directory in that directory. Run following command

   tar -xvzf apache-tomcat-8.0.32.tar.gz

   A directory called apache-tomcat-8.0.32 will be created.


DEPLOYING APPLICATION ON TOMCAT:
********************************

1. Copy the IITBcakupProcessor.war from 'installation_instructions/files' directory to /home/hduser/apache-tomcat-8.0.32/webapps directory.

2. Open a terminal on Unix/ Windows. Go to the DIRECTORY_OF_YOUR_CHOICE/apache-tomcat-8.0.32/bin directory

3. For Unix run the command ./startup.sh 

4. For Windows run startup.bat

5. It will start tomcat, 

RUNNING THE APPLICATION
************************

1. Run on browser localhost:8484/IITBackupProcessor

2. You will get a screen with the following menu

     > Extracting Course Structure (From Old Course MongoDb)
     > Extracting Course Structure (From Latest Course MongoDb)
     > Extracting User Info & Summary Records of Student's Engagement (From LMS edxapp DB)
     > Extracting Discussion & Forum Info (From Mongo comments Service)
     > Extracting Log Info (From log files)

3. If you are using openedx earlier version ( upto Aspen) use the 'Extracting Course Structure (From Old Course MongoDb)' else use    
   'Extracting Course Structure (From Latest Course MongoDb)'

4. Run all modules in the same sequential order as seen on the screen, because the later modules are dependant on previous modules.

5. You may shutdown tomcat by running shutdown.sh (unix), shutdown.bat (Windows)


CHANGING MYSQL USERID & PASSWORD IN THE APPLICATION :
******************************************************

1. After running starting Apache tomcat once, shut it down, open the following file

   /home/hduser/apache-tomcat-8.0.32/webapps/IITBackupProcessor/META-INF/context.xml, in the Resource section change the portion
   username="root" password="root123" to reflect your userid & root. Rerun tomcat

SETTING UP LOGFILE DIRECTORY SECTION
=====================================

1. open /home/hduser/apache-tomcat-8.0.32/webapps/IITBackupProcessor/WEB-INF/classes/EdxParams.properties file,
   Edit & put
   LOGDIRPATH= <YOUR_LOG_DIR_FULL_PATH>




