-- MySQL dump 10.13  Distrib 5.6.35, for Linux (x86_64)
--
-- Host: localhost    Database: IITBxDataAnalytics
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Current Database: `IITBxDataAnalytics`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `IITBxDataAnalytics` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `IITBxDataAnalytics`;

--
-- Table structure for table `Cities`
--

DROP TABLE IF EXISTS `Cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `stateId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10962 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Cities2`
--

DROP TABLE IF EXISTS `Cities2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cities2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `stateId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Course`
--

DROP TABLE IF EXISTS `Course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Course` (
  `courseId` int(11) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `courseTitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authorUserId` int(11) DEFAULT NULL,
  `currConcepts` longtext COLLATE utf8_unicode_ci,
  `prevConcepts` longtext COLLATE utf8_unicode_ci,
  `courseLang` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minPrice` int(11) DEFAULT NULL,
  `suggestedPrice` int(11) DEFAULT NULL,
  `currencyCode` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  PRIMARY KEY (`courseId`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseCategory`
--

DROP TABLE IF EXISTS `CourseCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseCategory` (
  `courseCatgid` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseCounts` int(3) DEFAULT NULL,
  `parentId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`courseCatgid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseChapter`
--

DROP TABLE IF EXISTS `CourseChapter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseChapter` (
  `chapterId` int(11) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chapteTitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chapterSysName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`chapterId`),
  KEY `courseChaptChapt` (`courseName`,`chapterSysName`)
) ENGINE=InnoDB AUTO_INCREMENT=4181 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseDiscussions`
--

DROP TABLE IF EXISTS `CourseDiscussions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseDiscussions` (
  `discussionId` int(11) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `chapterSysName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discussionTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discussionSysName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discussionSysId` varchar(45) DEFAULT NULL,
  `verticalSysName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`discussionId`)
) ENGINE=InnoDB AUTO_INCREMENT=1436 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseFiles`
--

DROP TABLE IF EXISTS `CourseFiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseFiles` (
  `fileId` int(11) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `chapterSysName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sessionSysName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fileTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fileSysName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`fileId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseForums`
--

DROP TABLE IF EXISTS `CourseForums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseForums` (
  `forumId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) DEFAULT NULL,
  `orgName` varchar(50) DEFAULT NULL,
  `courseName` varchar(50) DEFAULT NULL,
  `courseRun` varchar(50) DEFAULT NULL,
  `commentSysId` varchar(50) DEFAULT NULL,
  `commentType` varchar(45) DEFAULT NULL,
  `anonymousMode` varchar(1) DEFAULT NULL,
  `lmsAuthorId` bigint(20) DEFAULT NULL,
  `lmsAuthorName` varchar(120) DEFAULT NULL,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `lastModDateTime` timestamp NULL DEFAULT NULL,
  `upVoteCount` varchar(45) DEFAULT NULL,
  `totVoteCount` varchar(45) DEFAULT NULL,
  `commentCount` int(11) DEFAULT NULL,
  `threadType` varchar(50) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `commentableSysId` varchar(45) DEFAULT NULL,
  `endorsed` varchar(1) DEFAULT NULL,
  `closed` bit(1) DEFAULT NULL,
  `visible` bit(1) DEFAULT NULL,
  PRIMARY KEY (`forumId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseOpenAssess`
--

DROP TABLE IF EXISTS `CourseOpenAssess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseOpenAssess` (
  `openassess_id` int(11) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) DEFAULT NULL,
  `orgName` varchar(50) DEFAULT NULL,
  `courseName` varchar(50) DEFAULT NULL,
  `courseRun` varchar(50) DEFAULT NULL,
  `assessSysId` varchar(32) DEFAULT NULL,
  `prompt` longtext,
  `chapterSysName` varchar(50) DEFAULT NULL,
  `sessionSysName` varchar(50) DEFAULT NULL,
  `verticalSysName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`openassess_id`)
) ENGINE=InnoDB AUTO_INCREMENT=542 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseOthers`
--

DROP TABLE IF EXISTS `CourseOthers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseOthers` (
  `otherId` int(11) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmlSysName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `verticalSysName` varchar(50) DEFAULT NULL,
  `chapterSysName` varchar(50) DEFAULT NULL,
  `sessionSysName` varchar(50) DEFAULT NULL,
  `fileName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`otherId`)
) ENGINE=InnoDB AUTO_INCREMENT=28564 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseProblems`
--

DROP TABLE IF EXISTS `CourseProblems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseProblems` (
  `problemId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chapterSysName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessionSysName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quizSysName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quizTitle` longtext COLLATE utf8_unicode_ci,
  `quizType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quizWeight` float(6,2) DEFAULT NULL,
  `noOfAttemptsAllowed` int(3) DEFAULT NULL,
  `quizMaxMarks` float(6,2) DEFAULT NULL,
  `hintAvailable` smallint(6) DEFAULT NULL,
  `correctChoice` smallint(6) DEFAULT NULL,
  `hintMode` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verticalSysName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`problemId`)
) ENGINE=InnoDB AUTO_INCREMENT=43281 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseRun`
--

DROP TABLE IF EXISTS `CourseRun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseRun` (
  `courseRunid` int(11) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` int(50) NOT NULL,
  `courseRun` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `willbeGraded` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gradePass` float(5,2) DEFAULT NULL,
  `actualPrice` int(11) DEFAULT NULL,
  `currencyCode` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  PRIMARY KEY (`courseRunid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseSequential`
--

DROP TABLE IF EXISTS `CourseSequential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseSequential` (
  `sessionId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `chapterSysName` varchar(50) DEFAULT NULL,
  `sessionSysName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sessionTitle` varchar(255) DEFAULT NULL,
  `position` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`sessionId`),
  KEY `courseSeqSession` (`courseName`,`chapterSysName`,`sessionSysName`)
) ENGINE=InnoDB AUTO_INCREMENT=3102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseVersion`
--

DROP TABLE IF EXISTS `CourseVersion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseVersion` (
  `courseVersionid` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` int(11) NOT NULL,
  `Description` longtext COLLATE utf8_unicode_ci,
  `CreatedOn` date DEFAULT NULL,
  `LastModified` date DEFAULT NULL,
  `filePathName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`courseVersionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseVertical`
--

DROP TABLE IF EXISTS `CourseVertical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseVertical` (
  `vertId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) DEFAULT NULL,
  `orgName` varchar(50) DEFAULT NULL,
  `courseName` varchar(50) DEFAULT NULL,
  `sessionSysName` varchar(50) DEFAULT NULL,
  `verticalSysName` varchar(50) DEFAULT NULL,
  `chapterSysName` varchar(50) DEFAULT NULL,
  `verticalTitle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`vertId`),
  KEY `vertCourseSys` (`courseName`,`verticalSysName`)
) ENGINE=InnoDB AUTO_INCREMENT=41655 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseVideos`
--

DROP TABLE IF EXISTS `CourseVideos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseVideos` (
  `videoId` int(11) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) DEFAULT NULL,
  `orgName` varchar(50) DEFAULT NULL,
  `courseName` varchar(50) DEFAULT NULL,
  `chapterSysName` varchar(50) DEFAULT NULL,
  `videoSysName` varchar(255) DEFAULT NULL,
  `videoUTubeId` varchar(50) DEFAULT NULL,
  `videoDownload` tinyint(1) DEFAULT NULL,
  `videoTrackDownLoad` tinyint(1) DEFAULT NULL,
  `videoTitle` varchar(255) DEFAULT NULL,
  `videoUTubeId075` varchar(50) DEFAULT NULL,
  `videoUTubeId125` varchar(50) DEFAULT NULL,
  `videoUTubeId15` varchar(50) DEFAULT NULL,
  `videolength` float DEFAULT NULL,
  `verticalSysName` varchar(50) DEFAULT NULL,
  `sessionSysName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`videoId`)
) ENGINE=InnoDB AUTO_INCREMENT=1840 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CourseWiki`
--

DROP TABLE IF EXISTS `CourseWiki`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CourseWiki` (
  `wikiId` int(11) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `wikiSlug` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lmsWikiId` bigint(20) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModDate` datetime DEFAULT NULL,
  `lastRevId` int(11) DEFAULT NULL,
  `ownerId` bigint(20) DEFAULT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `groupRead` tinyint(4) DEFAULT NULL,
  `groupWrite` tinyint(4) DEFAULT NULL,
  `otherRead` tinyint(4) DEFAULT NULL,
  `otherWrite` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`wikiId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventAttributes`
--

DROP TABLE IF EXISTS `EventAttributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventAttributes` (
  `name` varchar(200) NOT NULL,
  `eventName` varchar(200) NOT NULL,
  PRIMARY KEY (`name`,`eventName`),
  KEY `eventName` (`eventName`),
  CONSTRAINT `eventattributes_ibfk_1` FOREIGN KEY (`eventName`) REFERENCES `EventName` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventCourseInteract`
--

DROP TABLE IF EXISTS `EventCourseInteract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventCourseInteract` (
  `eventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseRun` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) NOT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventNo` smallint(6) DEFAULT NULL,
  `moduleType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moduleSysName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `moduleTitle` longtext COLLATE utf8_unicode_ci,
  `chapterSysName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chapterTitle` longtext COLLATE utf8_unicode_ci,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `modDateTime` timestamp NULL DEFAULT NULL,
  `oldPosition` smallint(6) DEFAULT NULL,
  `curPosition` smallint(6) DEFAULT NULL,
  `source` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`eventId`),
  KEY `courseCourseInt` (`courseName`,`chapterSysName`,`moduleSysName`)
) ENGINE=InnoDB AUTO_INCREMENT=541228 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventDiscussion`
--

DROP TABLE IF EXISTS `EventDiscussion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventDiscussion` (
  `eventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventEnrollment`
--

DROP TABLE IF EXISTS `EventEnrollment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventEnrollment` (
  `ventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `userName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eduLevel` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activate` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adminUserId` bigint(20) DEFAULT NULL,
  `dateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventForumInteract`
--

DROP TABLE IF EXISTS `EventForumInteract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventForumInteract` (
  `eventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentThreadId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `queryText` longtext COLLATE utf8_unicode_ci,
  `noOfResults` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventInstructor`
--

DROP TABLE IF EXISTS `EventInstructor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventInstructor` (
  `eventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventName`
--

DROP TABLE IF EXISTS `EventName`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventName` (
  `name` varchar(200) NOT NULL,
  `checkStr` varchar(200) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `eventTypeName` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`name`,`checkStr`),
  KEY `eventTypename` (`eventTypeName`),
  CONSTRAINT `eventname_ibfk_1` FOREIGN KEY (`eventTypeName`) REFERENCES `EventType` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventNavigation`
--

DROP TABLE IF EXISTS `EventNavigation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventNavigation` (
  `eventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventNoType`
--

DROP TABLE IF EXISTS `EventNoType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventNoType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventNo` smallint(6) DEFAULT NULL,
  `eventTypeId` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventPDFInteract`
--

DROP TABLE IF EXISTS `EventPDFInteract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventPDFInteract` (
  `eventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventProbInteract`
--

DROP TABLE IF EXISTS `EventProbInteract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventProbInteract` (
  `eventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventNo` smallint(6) DEFAULT NULL,
  `quizzSysName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quizzTitle` longtext COLLATE utf8_unicode_ci,
  `chapterSysName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chapterTitle` longtext COLLATE utf8_unicode_ci,
  `hintAvailable` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hintMode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inputType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `responseType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `variantId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldScore` double DEFAULT NULL,
  `newScore` double DEFAULT NULL,
  `maxGrade` double DEFAULT NULL,
  `attempts` int(3) DEFAULT NULL,
  `maxAttempts` int(3) DEFAULT NULL,
  `choice` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `success` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probSubTime` timestamp NULL DEFAULT NULL,
  `done` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `lastModDateTime` timestamp NULL DEFAULT NULL,
  `courseRun` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB AUTO_INCREMENT=1881115 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventType`
--

DROP TABLE IF EXISTS `EventType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventType` (
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventVideoInteract`
--

DROP TABLE IF EXISTS `EventVideoInteract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventVideoInteract` (
  `eventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `sessionSysName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseRun` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventNo` smallint(6) DEFAULT NULL,
  `videoSysName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `videoTitle` longtext COLLATE utf8_unicode_ci,
  `chapterSysName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chapterTitle` longtext COLLATE utf8_unicode_ci,
  `oldSeekTime` float DEFAULT NULL,
  `currSeekTime` float DEFAULT NULL,
  `videoNavigType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldSpeed` float DEFAULT NULL,
  `currSpeed` float DEFAULT NULL,
  `source` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `lastModDateTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB AUTO_INCREMENT=195523 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EventWikiInteract`
--

DROP TABLE IF EXISTS `EventWikiInteract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EventWikiInteract` (
  `eventId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IITBxDataAnalytics.UserSessionOldLog`
--

DROP TABLE IF EXISTS `IITBxDataAnalytics.UserSessionOldLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IITBxDataAnalytics.UserSessionOldLog` (
  `sessionId` bigint(20) NOT NULL AUTO_INCREMENT,
  `sessionSysName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsName` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseRun` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `userName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hostName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipAddress` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `eventType` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventSource` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dataSource` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldVideoSpeed` float DEFAULT NULL,
  `currVideoSpeed` float DEFAULT NULL,
  `oldVideoTime` float DEFAULT NULL,
  `currVideoTime` float DEFAULT NULL,
  `videoNavigType` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oldGrade` float DEFAULT NULL,
  `currGrade` float DEFAULT NULL,
  `maxGrade` float DEFAULT NULL,
  `attempts` int(11) DEFAULT NULL,
  `maxNoAttempts` int(11) DEFAULT NULL,
  `hintAvailable` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hintUsed` longtext COLLATE utf8_unicode_ci,
  `currPosition` int(11) DEFAULT NULL,
  `oldPosition` int(11) DEFAULT NULL,
  `chapterSysName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chapterTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessSysName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moduleSysName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moduleTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `answerChoice` longtext COLLATE utf8_unicode_ci,
  `success` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `done` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enrolmentMode` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totDurationInSecs` int(20) DEFAULT NULL,
  `eventNo` smallint(6) DEFAULT NULL,
  `otherTitle` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otherType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anonymous` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anonymousToPeers` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eduLevel` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentableId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentSysId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aadhar` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `problemSubmissionTime` timestamp NULL DEFAULT NULL,
  `hintMode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currentSeekTime` float DEFAULT NULL,
  `queryText` longtext COLLATE utf8_unicode_ci,
  `noOfResults` smallint(6) DEFAULT NULL,
  `lastModDateTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`sessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LMSList`
--

DROP TABLE IF EXISTS `LMSList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LMSList` (
  `LMSShortName` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `LMSFullName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateTimeFormat` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`LMSShortName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `States`
--

DROP TABLE IF EXISTS `States`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `States` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `StudentAnswerDetails`
--

DROP TABLE IF EXISTS `StudentAnswerDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StudentAnswerDetails` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `userName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `chapterSysname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessionSysname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuestionId` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuestionType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuestionChoices` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `studentAnswers` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quizSysName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(2) DEFAULT NULL,
  `correctness` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `maxGrade` float DEFAULT NULL,
  `hintMode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hintUsed` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuesTitle` longtext COLLATE utf8_unicode_ci,
  `eventSource` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16490 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `StudentCourseAccessRole`
--

DROP TABLE IF EXISTS `StudentCourseAccessRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StudentCourseAccessRole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lmsUserId` int(11) NOT NULL,
  `orgName` varchar(64) NOT NULL,
  `courseName` varchar(255) NOT NULL,
  `courseRun` varchar(45) DEFAULT NULL,
  `role` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accessUser` (`lmsUserId`,`orgName`,`courseName`,`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `StudentCourseEnrolment`
--

DROP TABLE IF EXISTS `StudentCourseEnrolment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StudentCourseEnrolment` (
  `enrolId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(6) DEFAULT NULL,
  `orgName` varchar(50) DEFAULT NULL,
  `courseName` varchar(45) DEFAULT NULL,
  `courseRun` varchar(50) DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `enrolmentDate` timestamp NULL DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  `mode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`enrolId`)
) ENGINE=InnoDB AUTO_INCREMENT=324288 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `StudentCourseGrades`
--

DROP TABLE IF EXISTS `StudentCourseGrades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StudentCourseGrades` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseRun` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `lmsUserName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moduleType` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moduleSysName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `score` int(3) DEFAULT NULL,
  `maxScore` int(3) DEFAULT NULL,
  `noOfAttempts` int(2) DEFAULT NULL,
  `hintUsed` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hintAvailable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` longtext COLLATE utf8_unicode_ci,
  `goals` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `lastModDateTime` timestamp NULL DEFAULT NULL,
  `totSessDuraInSecs` int(11) DEFAULT NULL,
  `done` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `URLTree`
--

DROP TABLE IF EXISTS `URLTree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `URLTree` (
  `urlId` bigint(20) NOT NULL AUTO_INCREMENT,
  `lmsName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseRun` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlSysName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlType` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentUrl` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`urlId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `userId` bigint(10) NOT NULL AUTO_INCREMENT,
  `lmsUserId` bigint(20) NOT NULL,
  `lmsName` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registrationDate` date DEFAULT NULL,
  `emailId` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mothertounge` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `highestEduDegree` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goals` longtext COLLATE utf8_unicode_ci,
  `city` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` smallint(6) NOT NULL DEFAULT '0',
  `firstAccesDate` datetime DEFAULT NULL,
  `lastAccessDate` datetime DEFAULT NULL,
  `allowCert` smallint(6) DEFAULT NULL,
  `yearOfBirth` smallint(6) DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `aadharId` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `lmsUserId_UNIQUE` (`lmsUserId`)
) ENGINE=InnoDB AUTO_INCREMENT=140947 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserLogProbCheck`
--

DROP TABLE IF EXISTS `UserLogProbCheck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserLogProbCheck` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `lmsName` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `orgName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `userName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `chapterSysname` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessionSysname` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuestionId` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuestionType` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuestionChoices` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `studentAnswers` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `quizSysName` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(2) DEFAULT NULL,
  `correctness` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `maxGrade` float DEFAULT NULL,
  `hintMode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `hint` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `hintUsed` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuesTitle` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `eventSource` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserLogProbReset`
--

DROP TABLE IF EXISTS `UserLogProbReset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserLogProbReset` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `lmsName` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `orgName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `userName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `chapterSysname` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessionSysname` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuestionId` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuestionType` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuestionChoices` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `studentAnswers` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `quizSysName` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(2) DEFAULT NULL,
  `correctness` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `maxGrade` float DEFAULT NULL,
  `hintMode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `hint` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `hintUsed` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `subQuesTitle` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `eventSource` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserSessionLogNew`
--

DROP TABLE IF EXISTS `UserSessionLogNew`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSessionLogNew` (
  `sessionId` bigint(20) NOT NULL AUTO_INCREMENT,
  `sessionSysName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsName` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `orgName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseRun` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `userName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hostName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipAddress` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `eventType` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventSource` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `eventName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventNo` smallint(6) DEFAULT NULL,
  `enrolmentMode` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chapterSysName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chapterTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessSysName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moduleSysName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `moduleTitle` longtext COLLATE utf8_unicode_ci,
  `oldVideoSpeed` float DEFAULT NULL,
  `currVideoSpeed` float DEFAULT NULL,
  `oldVideoTime` float DEFAULT NULL,
  `currVideoTime` float DEFAULT NULL,
  `videoNavigType` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currCoursePosition` int(11) DEFAULT NULL,
  `oldCoursePosition` int(11) DEFAULT NULL,
  `grade` float DEFAULT NULL,
  `maxGrade` float DEFAULT NULL,
  `attempts` int(2) DEFAULT NULL,
  `maxNoAttempts` int(11) DEFAULT NULL,
  `problemSubmissionTime` timestamp NULL DEFAULT NULL,
  `success` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `done` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otherTitle` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otherType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discussSysId` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discussionTitle` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentSysId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentTitle` longtext COLLATE utf8_unicode_ci,
  `threadType` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anonymous` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anonymousToPeers` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forumQueryAssessResp` longtext COLLATE utf8_unicode_ci,
  `forumQueryResultNo` smallint(6) DEFAULT NULL,
  `endorseFlag` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastModDateTime` timestamp NULL DEFAULT NULL,
  `userVisitedDiscuss` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userFollowedDiscuss` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`sessionId`)
) ENGINE=InnoDB AUTO_INCREMENT=51598 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserSessionSQL`
--

DROP TABLE IF EXISTS `UserSessionSQL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSessionSQL` (
  `sessionId` bigint(20) NOT NULL DEFAULT '0',
  `lmsName` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `orgName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `courseRun` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lmsUserId` bigint(20) DEFAULT NULL,
  `userName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `hostName` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipAddress` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `eventType` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `eventNo` smallint(6) DEFAULT NULL,
  `enrolmentMode` varchar(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `chapterSysName` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `chapterTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessSysName` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `moduleSysName` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `moduleTitle` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `oldVideoSpeed` float DEFAULT NULL,
  `currVideoSpeed` float DEFAULT NULL,
  `oldVideoTime` float DEFAULT NULL,
  `currVideoTime` float DEFAULT NULL,
  `videoNavigType` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `currPosition` int(11) DEFAULT NULL,
  `oldPosition` int(11) DEFAULT NULL,
  `currentSeekTime` float DEFAULT NULL,
  `oldGrade` float DEFAULT NULL,
  `currGrade` float DEFAULT NULL,
  `maxGrade` float DEFAULT NULL,
  `attempts` int(11) DEFAULT NULL,
  `maxNoAttempts` int(11) DEFAULT NULL,
  `hintAvailable` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `hintUsed` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `hintMode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `answerChoice` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `problemSubmissionTime` timestamp NULL DEFAULT NULL,
  `success` varchar(255) DEFAULT NULL,
  `done` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `otherTitle` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `otherType` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discussSysId` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `discussionTitle` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentSysId` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentTitle` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `threadType` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentType` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `anonymous` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `anonymousToPeers` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `forumQueryText` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `forumQueryResultNo` smallint(6) DEFAULT NULL,
  `endorseFlag` varchar(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastModDateTime` timestamp NULL DEFAULT NULL,
  `userVisitedDiscuss` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `userFollowedDiscuss` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eventColor`
--

DROP TABLE IF EXISTS `eventColor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventColor` (
  `eventTYpeId` smallint(6) NOT NULL AUTO_INCREMENT,
  `eventType` varchar(45) DEFAULT NULL,
  `eventColor` varchar(45) DEFAULT NULL,
  `colorCode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`eventTYpeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eventNoTYpe`
--

DROP TABLE IF EXISTS `eventNoTYpe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventNoTYpe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventNo` smallint(6) DEFAULT NULL,
  `eventTypeId` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-17 11:32:24
